#!/bin/bash

SOURCE_DIR="/" 
BACKUP_BASE_DIR="/"  

declare -a BACKUP_DIRS=("backup1" "backup2" "backup3" "backup4")

LAST_BACKUP_FILE="${BACKUP_BASE_DIR}/.last_backup"
if [[ -f $LAST_BACKUP_FILE ]]; then
    LAST_BACKUP=$(cat $LAST_BACKUP_FILE)
else
    LAST_BACKUP=""
fi

NEXT_BACKUP=""
for i in "${!BACKUP_DIRS[@]}"; do
    if [[ "${BACKUP_DIRS[$i]}" == "$LAST_BACKUP" ]]; then
        NEXT_BACKUP="${BACKUP_DIRS[$(( (i+1) % 4 ))]}"
        break
    fi
done

if [[ -z "$NEXT_BACKUP" ]]; then
    NEXT_BACKUP="${BACKUP_DIRS[0]}"
fi

SOURCE_WIN_PATH=$(echo "$SOURCE_DIR" | sed 's/^\/mnt\/c/C:/')
BACKUP_WIN_PATH=$(echo "${BACKUP_BASE_DIR}/${NEXT_BACKUP}" | sed 's/^\/mnt\/c/C:/')

robocopy "$SOURCE_WIN_PATH" "$BACKUP_WIN_PATH" /MIR

echo "$NEXT_BACKUP" > $LAST_BACKUP_FILE

echo "Backup completed to ${BACKUP_BASE_DIR}/${NEXT_BACKUP}/"

