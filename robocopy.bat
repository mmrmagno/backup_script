@echo off
setlocal enabledelayedexpansion

set SOURCE_DIR=C:\Users\
set BACKUP_BASE_DIR=K:\

set LAST_BACKUP_FILE=!BACKUP_BASE_DIR!\backuptarget.txt
if exist !LAST_BACKUP_FILE! (
    set /p LAST_BACKUP=<!LAST_BACKUP_FILE!
) else (
	echo Ziel nicht gefunden
	echo !BACKUP_BASE_DIR!
	goto exit
)

if !LAST_BACKUP! gtr 4 (
	set /A NEXT_BACKUP=1
) else (
	set /A NEXT_BACKUP=!LAST_BACKUP!+1
)

echo !NEXT_BACKUP!

robocopy !SOURCE_DIR! !BACKUP_BASE_DIR!\backup!NEXT_BACKUP! /MIR /XD !SOURCE_DIR!\AppData
robocopy !SOURCE_DIR!\AppData !BACKUP_BASE_DIR!\backup!NEXT_BACKUP!\AppData\Roaming\Thunderbrird /MIR

echo !NEXT_BACKUP! > !LAST_BACKUP_FILE!

echo Backup completed to !BACKUP_BASE_DIR!backup!NEXT_BACKUP!\



:exit
pause